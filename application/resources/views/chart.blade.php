<html>
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Gochi+Hand" rel="stylesheet">
    <script src="https://balkangraph.com/js/latest/OrgChart.js"></script>
</head>
<style>
  html, body{
    width: 100%;
    height: 100%;
    padding: 0;
    margin:0;
    overflow: hidden;
    font-family: Helvetica;
  }
  #tree{
    width:100%;
    height:100%;
  }

  #editForm {
    display:none; 
    text-align:center; 
    position:absolute; 
    border: 1px solid #aeaeae;
    width:300px;
    background-color:#ffff;
    border-color: #ddd;
    z-index:10000; 
    color: #333;
  }

  #header-chart {
    padding: 10px 0 10px 0; 
    background-color:#17171145; 
    color: black;
  }
  
</style>
  <script src="https://balkangraph.com/js/latest/OrgChart.js"></script>
 
    <div id="editForm">
        <div id="header-chart">Update Commission</div>
        <div>
            <div style="padding: 10px 0 5px 0;">
                <label style="width:75px; display:inline-block;" for="name">Affiliate</label>
                <input id="name" value="" disabled="disabled" />
            </div>
            <div style="padding: 5px 0 5px 0;">
                <label style="width:75px; display:inline-block;" for="percentage">Comission</label>
                <input id="percentage" value="" />
            </div>
            <div style="padding: 5px 0 15px 0;">
                <button style="width:108px;" id="cancel">Cancel</button>
                <button style="width:108px;" id="save">Save</button>
            </div>
        </div>
    </div> 

<div id="tree"/>
</html>

<script type="text/javascript">
      var editForm = function () {
            this.nodeId = null;
        };

        editForm.prototype.init = function (obj) {
            var that = this;
            this.obj = obj;
            this.editForm = document.getElementById("editForm");
            this.nameInput = document.getElementById("name");
            this.percentageInput = document.getElementById("percentage");
            this.cancelButton = document.getElementById("cancel");
            this.saveButton = document.getElementById("save");

            this.cancelButton.addEventListener("click", function () {
                that.hide();
            });

            this.saveButton.addEventListener("click", function () {
            var node = chart.get(that.nodeId);
                node.name = that.nameInput.value;
                node.percentage = that.percentageInput.value;

                // add ajax
                console.log(node);

                chart.updateNode(node);
                that.hide();
            });
        };

        editForm.prototype.show = function (nodeId) {
            this.nodeId = nodeId;

            var left = document.body.offsetWidth / 2 - 150;
            this.editForm.style.display = "block";
            this.editForm.style.left = left + "px";
            var node = chart.get(nodeId);
            this.nameInput.value = node.name;
            this.percentageInput.value = node.percentage;
        };

        editForm.prototype.hide = function (showldUpdateTheNode) {
            this.editForm.style.display = "none";
        };


        var chart = new OrgChart(document.getElementById("tree"), {
          template: "olivia",
          nodeMenu:{
            edit: {text:"Edit"},
          },
          editUI: new editForm(),
          nodeMouseClick: OrgChart.action.none,
          enableSearch: false,
          nodeBinding: {
              field_0: "name",
            field_1: "percentage",
            img_0: "img",
            field_number_children: "field_number_children"
          }
        });   

        var nodes = [
          { id: 1, name: "Merwin Dale", percentage: "75%", img: "https://balkangraph.com/js/img/4.jpg" },
            { id: 2, pid: 1, name: "Kyla Chou", percentage: "Level 1 (50%)", img: "https://balkangraph.com/js/img/3.jpg" },
            { id: 3, pid: 1, name: "Caden Ellison", percentage: "Level 1(25%)", img: "https://balkangraph.com/js/img/4.jpg" },
            { id: 4, pid: 2, name: "Elliot Patel", percentage: "Level 2(15%)", img: "https://balkangraph.com/js/img/5.jpg" },
        ];

        // replace ajax data
        chart.load(nodes);

</script>