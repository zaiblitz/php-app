@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
	      <div class="x_title">
	        <h2>Players <small>search</small></h2>
	        <ul class="nav navbar-right panel_toolbox">
	          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	          </li>
	          <li class="dropdown">
	            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	            <ul class="dropdown-menu" role="menu">
	              <li><a href="#">Settings 1</a>
	              </li>
	              <li><a href="#">Settings 2</a>
	              </li>
	            </ul>
	          </li>
	          <li><a class="close-link"><i class="fa fa-close"></i></a>
	          </li>
	        </ul>
	        <div class="clearfix"></div>
	      </div>
	      <div class="x_content">
	        <br>
	        <form class="form-horizontal form-label-left input_mask">

	          <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
	            <input type="text" class="form-control has-feedback-left" id="inputSuccess2" placeholder="First Name">
	            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
	          </div>

	          <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
	            <input type="text" class="form-control" id="inputSuccess3" placeholder="Last Name">
	            <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
	          </div>

	          <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
	            <input type="text" class="form-control has-feedback-left" id="inputSuccess4" placeholder="Email">
	            <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
	          </div>

	          <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
	            <input type="text" class="form-control" id="inputSuccess5" placeholder="Phone">
	            <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
	          </div>

	           <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
	              <button type="button" class="btn btn-primary">Cancel</button>
				   <button class="btn btn-primary" type="reset">Reset</button>
	              <button type="submit" class="btn btn-success">Submit</button>
	            </div>

	        </form>
	      </div>
	    </div>
	</div>	

	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
	      <div class="x_title">
	        <h2>Player List <small>using handlebars plugin</small></h2>
	        <div class="clearfix"></div>
	      </div>
	      <div class="x_content" id="playerList">

	        <table class="table table-bordered">
	          <thead>
	            <tr>
	              <th>#</th>
	              <th>First Name</th>
	              <th>Last Name</th>
	              <th>Email Address</th>
	              <th>Username</th>
	            </tr>
	          </thead>
	          <tbody id="playerData">
	          	@foreach ($players as $player)
	          		<tr>
	          			<td>{{$player->id}}</td>
	          			<td>{{$player->first_name}}</td>
	          			<td>{{$player->last_name}}</td>
	          			<td>{{$player->email_address}}</td>
	          			<td>{{$player->username}}</td>
          			</tr>
	          	@endforeach
	          </tbody>
	        </table>
	      </div>
	    </div>
	</div>
</div>
@endsection


<script type="text/javascript">
	document.ready(function(){
		Handlebars.createTemplate({
			container: 'test',
			source_template: 'test',
			data: {
				list : 'Merwin'
			}
		});
	});
</script>