<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});



Route::get('/redis', 'TestController@redis');
Route::get('/rabbitmq', 'TestController@rabbitmq');
Route::get('/rabbitmq-consume', 'TestController@rabbitmq_consume');


Route::get('/roles-and-permission', 'TestController@roles_and_permission');


Route::get('/org-chart', 'TestController@organizational_chart');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/player-list', 'PlayerController@player_list');
