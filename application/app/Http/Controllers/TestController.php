<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Amqp;

// spati roles and permission
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class TestController extends Controller
{

	private static $key_time_expired = "00:01:00";  # 1min
    

    public function redis() {
    	$request_username = 'zai'; # should be unique (from post data)

    	$redis_key = $request_username;

    	// check if already exist key in redis. then get this
    	$user_token = Redis::hget($redis_key, 'user_token');
    	$user_id = Redis::hget($redis_key, 'external_account_id');

    	if(!empty($user_token) && !empty($user_id)) {
    		echo 'This is from redis cache '.$user_id;
    		die();
    	} 

    	// process token in api
    	$call_api = true;
    	$call_api_token = '12345_zai_token';
    	$call_external_account_id = 101089;

    	echo 'This is from api '.$call_external_account_id;

    	if($call_api) {
    		$redis_data = array(
    			'user_token' => $call_api_token,
    			'external_account_id' => $call_external_account_id
    		);

    		// set key in redis server
			Redis::pipeline(function ($pipe) use($redis_key, $redis_data){  
                $time_to_expired = $this->time_to_seconds(static::$key_time_expired);
                $pipe->hmset($redis_key, $redis_data)->expire($redis_key, $time_to_expired);  
            });
    	}
	
    }

    // convert time to seconds
    public function time_to_seconds($time) {
		list($h, $m, $s) = explode(':', $time);
		return ($h * 3600) + ($m * 60) + $s;
	}

    public function rabbitmq() {

    	$data = array(
    		'first_name' => 'Merwin Dale Domingo',
    		'age' => 29
    	);

    	$message = json_encode($data);

    	$content_type = 'application/json';
    	Amqp::publish('zai.key', $message, ['exchange' => 'zai.exchange', 'exchange_type' => 'direct', 'content_type' => $content_type]);

    	echo 'Success';
    }

    public function rabbitmq_consume() {
        $config = [
            'queue_name'=>'zai-queue', 
            'exchange'=>'zai.exchange',
            'exchange_type'=>'direct'
        ];    
  
        Amqp::consume($config['queue_name'], function ($message, $resolver) use($config) {
            $body = $message->body; 
            $data = json_decode($body);
          
            //dd($data);

          	// process data here # save to db etc
          	if(!empty($data)) {
          		$resolver->acknowledge($message);
          		echo 'Acknowledge';
          	}

        }, [
            'exchange' => $config['exchange'],
            'exchange_type' => $config['exchange_type'],
            'persistent' => true // required if you want to listen forever
        ]); 
      
    }

    public function roles_and_permission() {
        // create role
        // $role = Role::create(['name' => 'writer']);

        // create permission
        // $permission = Permission::create(['name' => 'edit articles']);
        // $permission = Permission::create(['name' => 'remove articles']);

        // assign permission to role
        $role->givePermissionTo($permission);
        $permission->assignRole($role);


        // check Database Seeding https://github.com/spatie/laravel-permission
    }


    public function organizational_chart() {
        return view('chart');
    }
}
