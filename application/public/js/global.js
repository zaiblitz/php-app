var Handlebars = {
    createTemplate : function(params) {
        var container = params.container;
        var source = document.getElementById(params.sourceTemplate).innerHTML;
        var template = Handlebars.compile(source);
        var itemTemplate = template(params.data);
        container.html(itemTemplate);
        if (params.callback) {
            params.callback();
        }
    }
};