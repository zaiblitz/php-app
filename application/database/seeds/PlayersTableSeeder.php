<?php

use Illuminate\Database\Seeder;

class PlayersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('players')->truncate();

        $player_list = [
        	[
        		'id' => 1,
        		'first_name' => 'Merwin Dale',
        		'middle_name' => 'Villanueva',
        		'last_name' => 'Domingo',
        		'email_address' => 'merwindale.domingo@gmail.com',
                'username' => 'zaiblitz',
        		'gender' => 'm',
        		'password' => 'password',
        		'vip_id' => 1,
        		'status' => 1,
        		'created_at' => \Carbon\Carbon::now(),
        		'updated_at' => \Carbon\Carbon::now()
        	],
        	[
        		'id' => 2,
        		'first_name' => 'Camille',
        		'middle_name' => 'de',
        		'last_name' => 'Luna',
        		'email_address' => 'camille.deluna@gmail.com',
                'username' => 'camille',
        		'gender' => 'f',
        		'password' => 'password',
        		'vip_id' => 1,
        		'status' => 1,
        		'created_at' => \Carbon\Carbon::now(),
        		'updated_at' => \Carbon\Carbon::now()
        	],
        ];

        DB::table('players')->insert($player_list);
    }
}
